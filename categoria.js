var mongoose = require('mongoose');

var categoriaSchema = mongoose.Schema({
    name:{
        type: String,
        require: true
    },
    create_date:{
        type: Date,
        default:Date.now
    }
});

var Categoria = module.exports = mongoose.model('lugares1', categoriaSchema);

//Get Genre
module.exports.getCategorias = function (callback, limit) {
    Categoria.find(callback).limit(limit);
}
