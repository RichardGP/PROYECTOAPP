var express = require('express')
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose')

Lugar = require('./lugares.js');
Categoria = require('./categoria.js');

mongoose.connect('mongodb://167.99.108.187/lugaresTuristicosDB');
var db = mongoose.connection;


app.get('/', function (req, res) {
    res.send('SERVIDOR FUNCIONANDO!');
});

app.get('/api/lugares', function(req, res){
   Lugar.getLugares(function(err, genres){
        if (err) {
            throw err;
        }
        res.json(genres);
    });

});
app.get('/api/categoria',function(req, res){
    Categoria.getCategorias(function(err, lugares){
        if (err) {
            throw err;
        }
        res.json(lugares);
    });
});

app.get('/api/lugares/:id', function(req, res) {
    Lugar.findById(req.params.id, function(error, lugar) {
        if (error) return res.status(500).send(error);
       res.json(lugar);
    });
});

app.get('/api/lugares/categoria/:idCategoria', function(req, res) {
   Lugar.find({idCategoria : req.params.idCategoria},function(err, lugares){
        res.status(200).send(lugares);
    });
});


app.set('port', process.env.PORT || 3000);


app.listen(app.get('port'), () =>{
    console.log('Everythnig is running');
});
