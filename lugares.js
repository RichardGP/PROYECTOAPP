var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var lugarSchema = Schema({
    tipoLugar:{
        //type: String, require: true
    	type: Schema.Types.ObjectId,
    	ref: 'lugares1'

    },
    nombre:{
        type: String,
        require: true
    },
    descripcion:{
        type: String,
        require: true
    },
    latitud:{
        type: Number,
        require: true
    },
    longitud:{
        type: Number,
        require: true
    },
    activo:{
    	type: Boolean,
    	require: true
    },
    create_date:{
        type: Date,
        default:Date.now
    }
});

var Lugar = module.exports = mongoose.model('lugares', lugarSchema);

//Get Lugares
module.exports.getLugares = function (callback, limit) {
    Lugar.find(callback).limit(limit);
    
}

